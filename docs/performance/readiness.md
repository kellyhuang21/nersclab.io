# Perlmutter Readiness

This page contains recommendations for application developers to "hit
the ground running" with upcoming system architectures such
as [Perlmutter](https://www.nersc.gov/systems/perlmutter/).

Testing of performance on relevant hardware and compatibility with the
software environment are both important.

## Perlmutter GPU information

* [Ampere in depth](https://devblogs.nvidia.com/nvidia-ampere-architecture-in-depth/)

## Accelerator porting recommendations

!!! fail "Write a validation test"
	Performance doesn't matter if you get the wrong answer!

	* https://doi.org/10.1086/342267
	* https://doi.org/10.1109/MCSE.2017.3971169

* Define benchmarks for performance. These should represent the
  science cases you want to run on Perlmutter.
* Use optimized libraries when possible (FFT, BLAS, etc).
* Start with 1 MPI rank per GPU.
* Start with UVM and add explicit data movement control as needed.
* Minimize data movement (Host to device and device to host transfers).
* Avoid device allocations (Use
  a [pool allocator](https://en.wikipedia.org/wiki/Memory_pool))

### Fortran

* Ensure code compiles with PGI compilers
* Port to OpenACC
* (optional for portability) Port OpenACC to OpenMP offload as
  compiler support for OpenMP 5.0+ matures.

!!! tip 
	With `use cutensorEx` PGI compilers will map Fortran
	intrinsics such as `MATMUL`, `RESHAPE`, `TRANSPOSE` AND `SPREAD`
	to [CuTENSOR](https://developer.nvidia.com/cutensor) library
	calls. ([Details](https://www.pgroup.com/resources/docs/20.4/x86/pgi-cuda-interfaces/index.htm#cftensor-cutensorex))

### C++

#### std

The C++ standard is continually evolving.

!!! Example
	* Parallel STL (pSTL) to achieve parallelism in many STL functions
          * [Intel's implementation](https://software.intel.com/content/www/us/en/develop/articles/get-started-with-parallel-stl.html)
          * [NVIDIA/PGI's roadmap for GPU acceleration](https://developer.download.nvidia.com/video/gputechconf/gtc/2019/presentation/s9770-c++17-parallel-algorithms-for-nvidia-gpus-with-pgi-c++.pdf)
	* [Executors](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2020/p0443r12.html) for heterogeneous programming and code execution
	* [mdspan](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2018/p0009r6.html) for multi-dimensional arrays
          * [Kokkos implementation](https://github.com/kokkos/mdspan)

Several libraries, frameworks and language extensions provide a path
towards fully standard based accelerated code:

* [kokkos](https://github.com/kokkos)
* [thrust](https://docs.nvidia.com/cuda/thrust/index.html)

Experimental (for CUDA devices)

* [SYCL / DPC++](https://github.com/intel/llvm/tree/sycl)
* [HIP](https://github.com/ROCm-Developer-Tools/HIP)

#### CUDA

While the standards based approach offers high portability maximum
performance can be obtained by specializing for specific hardware.

* [CUDA](https://developer.nvidia.com/cuda-toolkit)
* [CUB](https://nvlabs.github.io/cub/)

!!! tip
	With C++ it is possible to encapsulate such code through
	template specialization in order to preserve portability.

#### Directives

It is possible to use directives (OpenMP, OpenACC) with C++ code, but
this is not recommended. Directives are more suited to "C with
classes" style code than "modern C++".

As with Fortran it is recommended to start with OpenACC and PGI
compilers and then translate to OpenMP if desired.

## Proxy hardware platforms

Perlmutter will feature NVIDIA GPUs and AMD cpus.

### Cloud providers

* [AWS](https://aws.amazon.com/ec2/instance-types/p3/)
* [Google](https://cloud.google.com/compute/docs/gpus/)
* [Azure](https://docs.microsoft.com/en-us/azure/virtual-machines/windows/sizes-gpu)

### HPC systems

Summit and Sierra feature NVIDIA GPUs and IBM CPUs. Piz Daint features
NVIDIA GPUs and Intel CPUs, but only has 1 GPU per node.

* [Summit](https://www.olcf.ornl.gov/summit/)
* [Sierra](https://computation.llnl.gov/computers/sierra)
* [Piz Daint](https://www.cscs.ch/computers/dismissed/piz-daint-piz-dora/)

### CPU

Current generation AMD cpus are a good place to start.

* [AWS](https://aws.amazon.com/ec2/amd/)

## Software environment

Compilers and programming models play key roles in the software
environment.

* `module load pgi` on Cori.
* [PGI Compilers on AWS](https://www.pgroup.com/blogs/posts/pgi-ami-on-aws.htm)

## Programming Models

The choice of programming model depends on multiple factors including:
number of performance critical kernels, source language, existing
programming model, and portability of algorithm. A 20K line C++ code
with 5 main kernels will have different priorities and choices vs a
10M line Fortran code.

### Fortran

* [OpenMP](https://www.openmp.org)
* [CUDA Fortran](https://developer.nvidia.com/cuda-fortran)
* [OpenACC](https://www.openacc.org)

### C

* [CUDA](https://developer.nvidia.com/cuda-zone)
* [OpenMP](https://www.openmp.org)
* [OpenACC](https://www.openacc.org)

### C++

* [CUDA](https://developer.nvidia.com/cuda-zone)
* [Kokkos](https://github.com/kokkos/kokkos)
* [RAJA](https://github.com/LLNL/RAJA)
* [Thrust](https://thrust.github.io)
* [CUDA](https://developer.nvidia.com/cuda-zone)
* [OpenMP](https://www.openmp.org)
* [OpenACC](https://www.openacc.org)

### Python

There are many options for using Python on GPUs, each with their own set of
pros/cons. We have tried to provide a brief overview of several frameworks
here. The Python GPU landscape is changing quickly so please check back
periodically for more information.

#### CuPy

[CuPy](https://cupy.chainer.org/)
 is a drop-in replacement for NumPy. Where the NumPy internals
are often written in C and Fortran, the CuPy internals are in CUDA.

*Pros*: Very easy to use, minimal code changes required.

*Cons*: Will only work on NVIDIA GPUs, performance may not be fully optimized.

#### Numba CUDA

[Numba](https://numba.pydata.org/)
is a JIT compiler for Python. It can translate Python
into optimized CPU or GPU code. It is best suited for porting
specific kernels to the GPU. Numba supports both NVIDIA and AMD
GPUs, although we'll only discuss Numba CUDA here.

*Pros*: Write a mix of Python/CUDA that can easily run on a GPU.

*Cons*: Numba CUDA kernels look almost like CUDA. Limited Python functionality permitted.

#### PyOpenCL

[PyOpenCL](https://mathema.tician.de/software/pyopencl/)
provides a framework for directly accessing
OpenCL via Python. PyOpenCL is best suited for porting
specific kernels to the GPU.

*Pros*: OpenCL can run on many architectures (CPU and GPU) and is extremely portable.

*Cons*: Harder to install, limited profiling tool support. You have to
actually know and understand OpenCL or its close relatives, C or C++.

#### PyCUDA

[PyCUDA](https://mathema.tician.de/software/pycuda/),
like PyOpenCL, provides a framework for directly
accessing CUDA via Python. PyCUDA is best suited for porting
specific kernels to the GPU.

*Pros*: Gives you full access to CUDA. Much more powerful than Numba CUDA.

*Cons*: You have to actually know and understand CUDA or its close relatives, C or C++.

#### JAX

[JAX](https://github.com/google/jax),
designed with machine learning in mind, is several things. It is a drop-in
replacement for NumPy and also includes a Python
JIT compiler. It uses the XLA compiler which can write portable code.

*Pros*: Portable replacement for NumPy. JIT compiler transforms Python code
into portable (CPU/GPU) code that can run on many architectures.

*Cons*: JIT compiler is not as powerful as Numba. Your code might need major
changes, including non-intuitive loop and indexing syntax. Interface
is not as friendly as CuPy.

#### NVIDIA RAPIDS

[NVIDIA RAPIDS](https://rapids.ai/) is a set of tools that allow familiar
Python libraries like scikit-learn, pandas, and Dask to easily run on GPUs.

*Pros*: Very easy to use, minimal code changes required.

*Cons*: Will only work on NVIDIA GPUs.

## Algorithms

The ability for applications to achieve both portability and high
performance across computer architectures remains an open
challenge.

However there are some general trends in current and emerging HPC
hardware: increased thread parallelism; wider vector units; and deep,
complex, memory hierarchies.

In some cases a [performance portable](portability.md) algorithm can
realized by considering generic "wide vectors" which could map to
either GPU SIMT threads or CPU SIMD lanes.

## References and Events

* P3HPC Workshop at SC
* [2019 DOE Performance Portability](https://doep3meeting2019.lbl.gov)
* https://performanceportability.org
