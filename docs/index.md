# NERSC Technical Documentation

!!! tip "NERSC welcomes your contributions"
	These pages are hosted from a
	[git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
	[contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/master/CONTRIBUTING.md)
	are welcome!

	[Fork this repo](https://gitlab.com/NERSC/nersc.gitlab.io/-/forks/new)

**[docs.nersc.gov](https://docs.nersc.gov)** is a resource with the
technical details for users to make effective use
of [NERSC](https://nersc.gov)'s resources. For center news and
information visit the [NERSC Home page](https://nersc.gov) and for
interactive content visit [MyNERSC](https://my.nersc.gov).
