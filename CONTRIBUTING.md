# Contribution guide

This guide outlines how to contribute to this project and standards
which should be observed when adding to this repositiory.

## About

This repository contains NERSC technical documentation written in
Markdown which is converted to html/css/js with the
[mkdocs](http://www.mkdocs.org) static site generator. The
[theme](https://gitlab.com/NERSC/mkdocs-material) is a fork of
[mkdocs-material](https://github.com/squidfunk/mkdocs-material) with
NERSC customizations such as the colors.

## Rules

1.  Follow this [Markdown styleguide](https://github.com/google/styleguide/blob/3591b2e540cbcb07423e02d20eee482165776603/docguide/style.md).
1.  Do not commit large files (e.g. very high-res images, binary data, executables) [Image optimization](https://developers.google.com/web/fundamentals/performance/optimizing-content-efficiency/image-optimization)
1.  No commits directly to the master branch

### Merge Requests

NERSC staff who have submitted a successful Merge Request (MR) are added to
the list of the approvers. Please assign appropriate individuals to review
your merge request. If you are not sure leave don't assign the MR to anyone 
instead leave it unassigned and someone will review it. If you don't get a 
response within 1-2 days please notify someone or reach out to to `#docs` 
slack channel.

Please review the automated CI checks when working on an active MR.

- Pipeline: https://gitlab.com/NERSC/nersc.gitlab.io/pipelines
- Jobs: https://gitlab.com/NERSC/nersc.gitlab.io/-/jobs


## Setup

### Prerequisites

1. Anaconda or Python virtual env
2. git
3. gitlab account

### Clone repo

To get started, click the **Fork** button for the upstream repo https://gitlab.com/NERSC/nersc.gitlab.io/, 
this will create a local copy of the documentation under your gitlab user with the 
following url where `<USER>` is your gitlab account: 

```
https://gitlab.com/<USER>/nersc.gitlab.io
```

### SSH Clone

SSH clone is convenient as it allows user to push changes without typing their password.
The authentication is done by uploading your public key in GitLab. For more 
details see [GitLab SSH Tutorial](https://www.tutorialspoint.com/gitlab/gitlab_ssh_key_setup.htm).

```
git clone git@gitlab.com:<USER>/nersc.gitlab.io.git
```

### HTTPS Clone
 
```
git clone https://gitlab.com/<USER>/nersc.gitlab.io.git
```

### Install Dependencies (Optional)

You will need to install some dependencies in order to build documentation 
locally. This step is optional but highly encouraged. For purpose of 
demonstration we will setup a conda environment, but you are free to setup 
a python environment of choice.
 
```
cd nersc.gitlab.io
conda create -n docs pip
source activate docs
pip install -r requirements.txt
```

## Sync Branches (Recommended)

It is important to keep your `master` branch in sync if you are working on 
documentation locally. To get started you will need to do the following

1. Add a remote `upstream` to point to upstream repository

```
git remote add upstream git@gitlab.com:NERSC/nersc.gitlab.io.git
```

If you have got this far, you should see two remote endpoints as shown below

```
$ git remote -v
origin	git@gitlab.com:shahzebsiddiqui/nersc.gitlab.io.git (fetch)
origin	git@gitlab.com:shahzebsiddiqui/nersc.gitlab.io.git (push)
upstream	git@gitlab.com:NERSC/nersc.gitlab.io.git (fetch)
upstream	git@gitlab.com:NERSC/nersc.gitlab.io.git (push)
```

2. Checkout `master` and sync `master` branch locally from upstream endpoint

```
git checkout master
git pull upstream master
```

The changes in step 2  will sync `master` locally but your fork is not in sync,
in order to complete this action run the following:

```
git push origin master
```

Please don't use your `master` branch to make any changes, this branch is used
to sync changes from upstream because all merge requests get pushed to master 
branch. Instead, create a feature branch from master as follows:

```
git checkout -b <branchname>
```

## Setup Git Commit Author 

It is important, git knows your identity when you commit changes that go 
upstream. Every git commit will have an Author field that identifies the committer.
Shown below is an example

```
commit 882e4dff091745b57d64eab68b3701ffe0c45bf6
Author: Shahzeb Siddiqui <shahzebmsiddiqui@gmail.com>
Date:   Thu May 28 20:17:20 2020 +0000

    Additional details for viewing reservations
```

The Author field is controlled by `user.name` and `user.email` in git 
configuration, this file is typically part of `.git/config` or it could be setup
user home directory `$HOME/.git/config` that is configured for all user repos. 

First check if your git author is configured properly, you can do this
via `git config -l` or `git config user.name` and `git config user.email`. 

If these values are not set please consider executing the following command:

```
git config user.name <First> <Last>
git config user.email <email>
```

If configured properly, your next commit should see the Author field in
your git commit defined by values `user.name` and `user.email`

## How to

### Edit with live preview

Open a terminal session with the appropriate conda environment
activated, navigate to the root directory of the repository (where
`mkdocs.yml` is located) and run the command `mkdocs serve`. This will
start a live-reloading local web server. You can then open
[http://127.0.0.1:8000](http://127.0.0.1:8000) in a web browser to
see your local copy of the site.

In another terminal (or with a GUI editor) edit existing files, add
new pages to `mkdocs.yml`, etc. As you save your changes the local
web serve will automatically rerender and reload the site.

### Output a static site

To build a self-contained directory containing the full html/css/js
of the site:

```
mkdocs build
```

### Contribute to the repo

#### Option 1: Work in upstream branch

Work with a branch of the main repo.

1.  Make a new branch and call it something descriptive.

    ```shell
    git checkout -b username/what_you_are_doing
    ```

2.  Create/edit your content
3.  Commit your changes

    ```
    git commit -m 'describe what I did'
    ```

4.  Push your changes to gitlab

    ```shell
    git push
    ```

    Or if the branch doesn't exist on the gitlab repository yet

    ```shell
    git push --set-upstream origin username/my-new-feature
    ```

5.  Check if the continuous integration of your changes was successful
    
    * It is possible the GitLab shared runners will fail for an opaque 
      reason (e.g. issues with the cloud provider where they are hosted).
      Hitting the "Retry" button for specific stages in the
      pipeline in the GitLab.com GUI may resolve this in some cases.

6.  Submit a merge request to the master branch with your changes

#### Option 2: Work in your Fork

Make a fork of the repository and do all of your work on the fork.
Submit a merge request through gitlab when you have made your changes.

#### Option 3: Work in GitLab Browser

For some changes you do not need the full environment. It is possible
to edit Markdown files directly on gitlab. This work should be in a
private fork or branch and submitted as a merge request. A new branch
can be created by clicking the "+" button next to the repository name.

For more details on how to contribute back using GitLab Web Editor checkout 
the following links: 
- https://docs.gitlab.com/ee/user/project/repository/web_editor.html
- https://docs.gitlab.com/ee/user/project/web_ide/

### Add a new page

For a newly added page to appear in the navigation edit the top-level
`mkdocs.yml` file.

### Review a Merge Request from a private fork

1.  Modify `.git/config` so merge requests are visible

    ```text
    ...
    [remote "origin"]
	        url = git@gitlab.com:NERSC/documentation.git
	        fetch = +refs/heads/*:refs/remotes/origin/*
	        fetch = +refs/merge-requests/*/head:refs/remotes/origin/pr/*
	...
	```

2.  Check for any Merge Requests

    ```shell
    git fetch
    ```

3.  Checkout a specific Merge Request for review (merge request `N`
	in this example)

    ```shell
    git checkout origin/pr/N
    ```

### Install and run markdown lint

In the base directory of the repository run `npm install
markdownlint-cli`. (You may need to
install [nodejs](https://nodejs.org/en/)).

```
git clone git@gitlab.com:NERSC/nersc.gitlab.io.git
cd nersc.gitlab.io
npm install markdownlint-cli
```

The linter is now installed locally and can be run

```
$ ./node_modules/markdownlint-cli/markdownlint.js docs/index.md
docs/index.md: 8: MD009/no-trailing-spaces Trailing spaces [Expected: 0 or 2; Actual: 1]
docs/index.md: 9: MD009/no-trailing-spaces Trailing spaces [Expected: 0 or 2; Actual: 1]
```

!!! note
	It is important to run the linter from the base directory so
	that the correct configutation file (`.markdownlint.json`) is
	used.

# Content standards

## Command prompts

1. when showing a command and sample result, include a prompt
   indicating where the command is run, eg for a command valid on any
   NERSC system, use `nersc$`:

    ```console
    nersc$ sqs
    JOBID   ST  USER   NAME         NODES REQUESTED USED  SUBMIT               PARTITION SCHEDULED_START      REASON
    864933  PD  elvis  first-job.*  2     10:00     0:00  2018-01-06T14:14:23  regular   avail_in_~48.0_days  None
    ```

    But if the command is cori-specific, use `cori$`:
    ```console
    cori$ sbatch -Cknl ./first-job.sh
    Submitted batch job 864933
    ```

2. Where possible, replace the username with `elvis`
(i.e. a clearly-arbitrary-but-fake user name)

3. If pasting a snippet of a long output, indicate cuts with `[snip]`:
    ```console
    nersc$ ls -l
    total 23
    drwxrwx--- 2 elvis elvis  512 Jan  5 13:56 accounts
    drwxrwx--- 3 elvis elvis  512 Jan  5 13:56 data
    drwxrwx--- 3 elvis elvis  512 Jan  9 15:35 demo
    drwxrwx--- 2 elvis elvis  512 Jan  5 13:56 img
    -rw-rw---- 1 elvis elvis  407 Jan  9 15:35 index.md
    [snip]
    ```

## Writing Style

When adding a page think about your audience.

* Are they new or advanced expert users?
* What is the goal of this content?

* [Grammatical Person](https://en.wikiversity.org/wiki/Technical_writing_style#Grammatical_person)
* [Active Voice](https://en.wikiversity.org/wiki/Technical_writing_style#Use_active_voice)

## Shell code should be `bash` not `csh`

## Definitions

* I/O not IO
* Slurm allocation
* NERSC allocation

## Slurm options

* Show both long and short option when introducing an option in text
* Use the long version (where possible) in scripts

## Markdown lint

Install the markdown linter (requires node/npm) locally
```shell
npm install markdownlint-cli
```

Run the linter from the base directory of the repository

```shell
./node_modules/markdownlint-cli/markdownlint.js docs
```
