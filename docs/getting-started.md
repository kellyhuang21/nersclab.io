# Welcome to NERSC

Welcome to the National Energy Research Scientific Computing Center
(NERSC)!

!!! success "About this page"
	This document will guide you through the basics of using NERSC's
	supercomputers, storage systems, and services.

## What is NERSC?

[NERSC](https://www.nersc.gov) provides High Performance Computing and
Storage facilities and support for research sponsored by, and of
interest to, the U.S.  Department of Energy (DOES) Office of Science
(SC). NERSC has the unique programmatic role of supporting all six
Office of Science program offices: Advanced Scientific Computing
Research, Basic Energy Sciences, Biological and Environmental
Research, Fusion Energy Sciences, High Energy Physics, and Nuclear
Physics.

Scientists who have been awarded research funding by any of the
offices are eligible to apply for an allocation of NERSC
time. Additional awards may be given to non-DOE funded project teams
whose research is aligned with the Office of Science's
mission. Allocations of time and storage are made by DOE.

NERSC is a national center, organizationally part of [Lawrence
Berkeley National Laboratory](https://www.lbl.gov) in Berkeley, CA.
NERSC staff and facilities are primarily located at Berkeley Lab's
Shyh Wang Hall on the Berkeley Lab campus.

## Computing & Storage Resources

### Cori

Cori is a [Cray XC40](https://www.cray.com/products/computing/xc-series)
supercomputer with approximately 12000 compute nodes.

* [Detailed system specifications](./systems/cori/index.md)
* [Live status](https://www.nersc.gov/live-status/motd/)

### Community File System (CFS)

The [Community File System (CFS)](./filesystems/community.md) is a
global file system available on all NERSC computational systems. It
allows sharing of data between users, systems, and the "outside
world".

* [Detailed usage information](./filesystems/community.md)
* [Live status](https://www.nersc.gov/live-status/motd/)

### HPSS (High Performance Storage System) Archival Storage

The High Performance Storage System (HPSS) is a modern, flexible,
performance-oriented mass storage system. It has been used at NERSC
for archival storage since 1998. HPSS is intended for long term
storage of data that is not frequently accessed.

* [Detailed usage information](./filesystems/archive.md)
* [Live status](https://www.nersc.gov/live-status/motd/)

## NERSC Accounts

In order to use the NERSC facilities, you need:

1. Access to an allocation of computational or storage resources as a
   member of a project
2. A user account with an associated user login name (also called
   username).

* [Obtaining an account](./accounts/index.md#obtaining-an-account)
* [NERSC Allocations](https://www.nersc.gov/users/accounts/allocations/)
* [Iris](https://iris.nersc.gov): Account and allocation management web interface
* [Password rules](./accounts/passwords.md)

!!! tip "With [Iris](https://iris.nersc.gov) you can"
	* check allocation balances
	* change passwords
	* run reports
	* update contact information
	* clear login failures
	* change login shell
	* and more!

## Connecting to NERSC

!!! check "MFA is *required* for NERSC users"

* [Multi-Factor Authentication (MFA)](./connect/mfa.md)
* [SSH](./connect/ssh.md)
* [Cori login nodes](./connect/login-nodes.md)
* [Troubleshooting connection problems](./connect/ssh.md#troubleshooting)
* [Live status](https://www.nersc.gov/live-status/motd/)

## NERSC Users Group (NUG)

Join the [NERSC Users Group](https://www.nersc.gov/users/NUG/): an
independent organization of users of NERSC resources.

!!! tip
	NUG maintains a Slack workspace that all users are welcome to
	[join](https://www.nersc.gov/users/NUG/nersc-users-slack/).

## Software

NERSC and its vendors supply a rich set of HPC utilities,
applications, and programming libraries. 

* [NERSC Supported Software Status List](./policies/software-policy/software_state.md).
* Application specific documentation on this site.
* Available [modules](./environment/index.md#nersc-modules-environment)
  login to Cori and run `module avail`)

!!! question "Something missing?"
	If there is something missing that you would like to have on
	our systems, please [submit a request](https://help.nersc.gov) and
	we will evaluate it for appropriateness, cost, effort, and benefit
	to the community.

## Computing Environment

!!! info
	[`$HOME` directories](./filesystems/global-home.md) are shared
	across all NERSC systems (except HPSS)

* [Customizing your environment](./environment/index.md)

## Compiling/ building software

* [compilers at NERSC](./development/compilers/wrappers.md)
* [environment](./environment/index.md)

## Running Jobs

Typical usage of the system involves submitting scripts (also
referred to as "jobs") to a batch system such as
[Slurm](https://slurm.schedmd.com/).

* [overview of jobs at NERSC](./jobs/index.md)
* [rich set of examples](./jobs/examples/index.md)

## Interactive Computing

NERSC also supports interactive computing.

* [Interactive jobs](./jobs/interactive.md)
* [Jupyter](./services/jupyter.md)

## Transferring Data

NERSC provides several ways to transfer data both inside and outside
of NERSC.

* [Transferring data](./data/transfer.md) Tools and recommendations.
* [Data transfer nodes](./systems/dtn/index.md) Optimized for transfers.

## Getting Help

NERSC places a very strong emphasis on enabling science and providing
user-oriented systems and services.

### Documentation

NERSC maintains extensive [documentation](https://docs.nersc.gov). 

!!! check "NERSC welcomes your contributions"
    These pages are hosted from a
    [git repository](https://gitlab.com/NERSC/nersc.gitlab.io) and
    [contributions](https://gitlab.com/NERSC/nersc.gitlab.io/blob/master/CONTRIBUTING.md)
    are welcome!

    [Fork this repo](https://gitlab.com/NERSC/nersc.gitlab.io/-/forks/new)

### Account support

* [Online Help Desk](https://help.nersc.gov) (preferred, login required)
* [email](mailto:accounts@nersc.gov) (if you cannot login)

!!! info "Availability"
	Account support is available 8-5 Pacific Time on business days.

### Consulting

NERSC's consultants are HPC experts and can answer just about all of
your technical questions.

* [Online Help Desk](https://help.nersc.gov)

!!! info "Availability"
	Account support is available 8-5 Pacific Time on business days.

### Operations

!!! warn "For *critical* system issues only."

* 1-800-666-3772 (USA only) or 1-510-486-8600, Option 1
